$(function () {
    $('#msgform').submit(function (e) {
        e.preventDefault();
        let msg = $('#msg').val();

        $.ajax({
            url: "/rest/chat",
            type: "post",
            data: {
                text: msg
            },
            dataType: "json",
            success: function (response) {
                $('#messages').append($('<li>').text('VA: ' + response));
            },
            error: function (xhr) {
                console.error(xhr);
            }
        });

        $('#msg').val('');
        $('#messages').append($('<li>').text('ME: ' + msg));
        return false;
    });
});