var socket = io();
$(function () {
    $('#msgform').submit(function (e) {
        e.preventDefault();
        let msg = $('#msg').val();
        socket.on(socket.id).emit('message', msg);
        $('#msg').val('');
        $('#messages').append($('<li>').text('ME: ' + msg));
        return false;
    });

    socket.on('message', function (msg) {
        $('#messages').append($('<li>').text('VA: ' + msg));
    });
});