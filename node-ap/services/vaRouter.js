const VASkillService = require('./vaSkill');
const BaseVAService = require('./vaBase');
const container = require("typedi").Container;

const config = require('../config/ibm_watson.json')[process.env.NODE_ENV || 'dev'];

class VARouterService extends BaseVAService {
    constructor() {
        super(config.router);
        //TODO
        //let chainOrder = config.chain_order;
        config.skills.forEach(skill => {
            container.set(skill.intent, new VASkillService(skill));
        });
    }

    async message(msg, id) {
        let vaskill = BaseVAService.getSkill(id);
        if (vaskill) {
            console.debug("reusing seesion/skill: " + id);
            return await vaskill.chat(msg, id);
        }

        let response;
        try {
            response = await this.service
                .message({
                    assistantId: this.assistant_id,
                    sessionId: this.idSessionMap[id],
                    input: {
                        'message_type': 'text',
                        'text': msg
                    }
                });
        } catch (error) {
            this.errorHandler(error);
            return error;
        }

        let output = response.result.output;
        if (this.isEmptyIntent(output)) {
            console.debug("routing ending with anything else");
            // can we reuse router session?
            // await this.deleteSession(id);
            return response;
        }

        output['intents'].forEach(intent => {
            let intentText = intent.intent;
            vaskill = container.get(intentText);
            if (vaskill) {
                BaseVAService.putSkill(id, vaskill);
                return;
            }
        });

        if (!vaskill) {
            console.error("unconfig intent skill");
            return response;
        }
        return await vaskill.chat(msg, id);
    };
}

module.exports = VARouterService;