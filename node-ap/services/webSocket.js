const container = require("typedi").Container;
const vaRouter = container.get(require('../services/vaRouter'));

class WebSocketService {
    constructor() {}

    connect() {
        global.io.on('connection', function (socket) {
            console.debug('a user connected: ' + socket.id);
        
            socket.on('disconnect', function () {
              console.debug('user disconnected:' + socket.id);
            });
        
            socket.on('message', async (msg) => {
              console.debug('user msg: ' + msg + ' socket.id: ' + socket.id);
              let response = await vaRouter.chat(msg, socket.id);
              let output = response.result.output;
              let text = '';
              output['generic'].forEach(generic => {
                text += generic.text + '\n';
              });
              text = text.slice(0, -1);
              io.to(socket.id).emit('message', text);
            });
        
          });
    }
}

module.exports = WebSocketService;