const VABaseService = require('./vaBase');

const container = require("typedi").Container;
const dialogDao = container.get(require('../dao/dialogDao'));
const DialogVO = require('../models/dialogVO');

class VASkillService extends VABaseService {
    constructor(config) {
        super(config);
        this.skill = config.intent
        this.nextSkillKey = config.next;
    }

    async message(msg, id) {
        let response;
        try {
            response = await this.service
                .message({
                    assistantId: this.assistant_id,
                    sessionId: this.idSessionMap[id],
                    input: {
                        'message_type': 'text',
                        'text': msg
                    }
                });
        } catch (error) {
            this.errorHandler(error);
            return error;
        }

        let output = response.result.output;
        if (this.isEmptyIntent(output)) {
            console.debug("skill ending with anything else: " + this.assistant_id);
            await this.deleteSession(id);
            VABaseService.deleteSkill(id);

            if (!this.nextSkillKey) {
                return response;
            }

            console.debug("chain to next skill: " + this.nextSkillKey);
            let nextSkill = container.get(this.nextSkillKey);
            return await nextSkill.chat(msg, id);
        }

        let vo = new DialogVO();
        vo.skill = this.skill
        vo.userId = id;
        vo.userSay = msg;
        vo.resMsg = response;
        vo.timestamp = Date.now();

        dialogDao.insertLog(vo);
        return response;
    };
}

module.exports = VASkillService;