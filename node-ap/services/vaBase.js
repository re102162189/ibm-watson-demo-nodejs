const AssistantV2 = require('ibm-watson/assistant/v2');
const { IamAuthenticator } = require('ibm-watson/auth');

const idSkillMap = {};

class VABaseService {
    constructor(config) {
        this.assistant_id = config.assistant_id;
        this.service = new AssistantV2({
            version: config.version,
            authenticator: new IamAuthenticator({
                apikey: config.api_key,
            }),
            url: config.url
        });
        this.idSessionMap = {};
    }

    async chat(msg, id) {
        if (id in this.idSessionMap) {
            return await this.message(msg, id);
        }

        try {
            let response = await this.service
                .createSession({
                    assistantId: this.assistant_id
                });
            this.idSessionMap[id] = response.result.session_id;
            return await this.message(msg, id);
        } catch (error) {
            console.error(error);
            return error;
        }
    }

    async deleteSession(id) {

        if(!this.idSessionMap[id]) {
            return;
        }

        try {
            await this.service
                .deleteSession({
                    assistantId: this.assistant_id,
                    sessionId: this.idSessionMap[id],
                });
            delete this.idSessionMap[id];
        } catch (error) {
            this.errorHandler(error);
            return error;
        }
    }

    errorHandler(err) {
        console.error(err);
        delete this.idSessionMap[id];
    }

    isEmptyIntent(output) {
        return 'intents' in output && output['intents'].length === 0 &&
        'entities' in output && output['entities'].length === 0;
    }

    static deleteSkill(id) {
        console.log("delete skill by id: " + id);
        delete idSkillMap[id];
    }

    static putSkill(id, skill) {
        console.log("put skill by id: " + id);
        idSkillMap[id] = skill;
    }

    static getSkill(id) {
        console.log("get skill by id: " + id);
        return idSkillMap[id];
    }
}

module.exports = VABaseService;