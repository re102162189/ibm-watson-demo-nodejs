const app = require('express')();
const container = require("typedi").Container;
const webSocket = container.get(require('../services/webSocket'));

app.get('/index', async (req, res) => {
  res.render("index", { channel : "ws" });
});

app.get('/vabot', async (req, res) => {
  let version = req.query.version;
  let containterId = req.query.containterId;
  console.debug('version=' + version);
  console.debug('containterId=' + containterId);

  res.render('vabot_ws', { containter : containterId });
  webSocket.connect();
});

app.get('/chat', async (req, res) => {
  res.render('ws');
  webSocket.connect();
});

module.exports = app;