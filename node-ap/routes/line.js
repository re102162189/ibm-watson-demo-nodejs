const app = require('express')();

const config = require('../config/line.json')[process.env.NODE_ENV || 'dev'];
const line = require('@line/bot-sdk');

const container = require("typedi").Container;
const vaRouter = container.get(require('../services/vaRouter'));

const client = new line.Client(config);

app.post('/webhook', line.middleware(config), async (req, res) => {

    res.status(200).send();

    let event = req.body.events[0];
    if (event.type !== 'message' || event.message.type !== 'text') {
        return Promise.resolve(null);
    }

    let msg = event.message.text;
    let userId = event.source.userId;

    let response = await vaRouter.chat(msg, userId);
    let output = response.result.output;
    let text = '';
    output['generic'].forEach(generic => {
        text += generic.text + '\n';
    });
    text = text.slice(0, -1);

    client.replyMessage(event.replyToken, {
        type: 'text',
        text: text,
    }).then(res => {
        console.log(res)
    }).catch(err => {
        console.log(err)
    });
});

module.exports = app;