const app = require('express')();
const bodyParser = require('body-parser');

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

const session = require('express-session');
app.use(session(
  {
    secret: '1ayhsn37a',
    resave: false,
    saveUninitialized: true,
    maxAge: null,
    cookie:
    {
      httpOnly: true,
    }
  }
));

const container = require("typedi").Container;
const vaRouter = container.get(require('../services/vaRouter'));

app.get('/index', async (req, res) => {
  res.render("index", { channel: "rest" });
});

app.get('/vabot', async (req, res) => {
  let version = req.query.version;
  let containterId = req.query.containterId;
  console.debug('version=' + version);
  console.debug('containterId=' + containterId);
  res.render('vabot_rest', { containter: containterId });
});

/**
 * @route POST /chat
 * @group rest - rest about VA
 * @param {string} msg.query.required - message
 * @produces application/json
 */
app.post('/chat', async (req, res) => {
  let msg = req.body.text;
  let httpSession = req.session.id;
  let response = await vaRouter.chat(msg, httpSession);
  let output = response.result.output;
  let text = '';
  output['generic'].forEach(generic => {
    text += generic.text + '\n';
  });
  text = text.slice(0, -1);
  res.json(text);
});

app.get('/chat', async (req, res) => {
  let msg = req.query.text;
  let httpSession = req.session.id;
  let response = await vaRouter.chat(msg, httpSession);
  let output = response.result.output;
  let text = '';
  output['generic'].forEach(generic => {
    text += generic.text + '\n';
  });
  text = text.slice(0, -1);
  res.json(text);
});

module.exports = app;