
class DialogVO {

    constructor() {
    }

    get skill() {
        return this._skill;
    }

    set skill(value) {
        this._skill = value;
    }

    get userId() {
        return this._userId;
    }

    set userId(value) {
        this._userId = value;
    }

    get userSay() {
        return this._userSay;
    }

    set userSay(value) {
        this._userSay = value;
    }

    get resMsg() {
        return this._resMsg;
    }

    set resMsg(value) {
        this._resMsg = value;
    }

    get timestamp() {
        return this._timestamp;
    }

    set timestamp(value) {
        this._timestamp = value;
    }
}

module.exports = DialogVO;