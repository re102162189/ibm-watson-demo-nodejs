const mongoose = require('mongoose');
const config = require('../config/mongo.json')[process.env.NODE_ENV || 'dev'];

class DialogDao {

    constructor() {
        mongoose.connect(config.url, { useUnifiedTopology: true, useNewUrlParser: true });
        this.db = mongoose.connection
        this.db.once('open', function () {
            console.debug("Connection Successful!");
        });
        this.dialog_col = config.dialog_col;
    }

    insertLog(dialogObj) {
        this.db.collection(this.dialog_col).insertOne(dialogObj);
        console.debug("insert");
    }
}

module.exports = DialogDao;