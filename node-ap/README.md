**Test url restful url **  
  
```bash
curl http://localhost:3000/rest/chat?text=i%20would%20like%20to%20make%20an%20appointment  
curl http://localhost:3000/rest/chat?text=tomorrow  
curl http://localhost:3000/rest/chat?text=noon  
curl http://localhost:3000/rest/chat?text=yes  
curl http://localhost:3000/rest/chat?text=123-456-7890  
curl http://localhost:3000/rest/chat?text=thanks
```

**Test with docker**  

*build services as docker image/container, see details in docker-compose.yml and Dockerfile*
  
```
docker-compose up
```

**Test url with docker**  
*reverse proxy to nodejs*
http://localhost/ws/chat (http://localhost/ws/chat -> http://localhost:3000/ws/chat)  

**get static file such as image using nginx**  
http://localhost/images/test.png

**Test url with ngrok (for LINE webhook)**  
ngrok http 3000  

**DO SETUP your ibm watson API and LINE bot**